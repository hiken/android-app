var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error("stuck",err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        let scriptSQL = `
        CREATE TABLE Subject(
            id INTEGER,
            name VARCHAR2(100), 
            CONSTRAINT PK_Subject PRIMARY KEY (id)
            )
        
        `
        db.run(`CREATE TABLE WorkSession(
            id INTEGER,
            name VARCHAR2(100),
            openingDate DATETIME, 
            timeSpend INTEGER,
            semaine INTEGER,
            subject_id INTEGER,
            CONSTRAINT PK_WorkSession PRIMARY KEY (id),
            CONSTRAINT FK_WorkSession_Subject FOREIGN KEY (subject_id) REFERENCES Subject (id)
            )`,(err) => {
                if (err) {
                    // Table already created
                }else{ 
                    // Table just created, creating some rows
                    // var insert = 'INSERT INTO Subject ( name) VALUES (?)'
                     let insertWork = "INSERT INTO WorkSession ( name,openingDate) VALUES (?,?)"
                    // db.run(insert, ["WEB"])
                     db.run(insertWork, ["user",Date.now()])
                }
            });
            
        db.run(scriptSQL,
        (err) => {
            if (err) {
                // Table already created
            }else{ 
                // Table just created, creating some rows
                 var insert = 'INSERT INTO Subject ( name) VALUES (?)'
                // let insertWork = "INSERT INTO WorkSession ( name,openingDate) VALUES (?,?)"
                 db.run(insert, ["WEB"])
                // db.run(insertWork, ["user",Date.now()])
            }
        });  
    }
});


module.exports = db