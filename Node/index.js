// lien du tuto
// https://developerhowto.com/2018/12/29/build-a-rest-api-with-node-js-and-express-js/
/*
Pour tester l'api,
il faut que tu aies installer node.js et npm 
link node: https://nodejs.org/en/
link : npm i npm 
Pour pouvoir lancer ce fichier, il faut que tu fasses la commande node index.js
Pour tester les routes (api.get , api.post),=> http://localhost:8000/api/users

*/
var express = require("express")
var app = express()
var db = require("./database.js")
var md5 = require("md5")

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var HTTP_PORT = 8000

// Start server
app.listen(HTTP_PORT, () => {
    // demarage du server avec node index.js
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// GET USERS 
// 
app.get("/api/getAllSubject", (req, res, next) => {
    var sql = "select * from subject"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});
app.get("/api/getAllSession", (req, res, next) => {
    var sql = "select * from WorkSession"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

app.get("/api/getWork", (req, res, next) => {
    var sql = "select subject.name as matiereName,WorkSession.name,WorkSession.id,WorkSession.semaine,WorkSession.openingDate,WorkSession.timeSpend,WorkSession.subject_id from WorkSession ,subject where WorkSession.subject_id == subject.id"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});
app.get("/api/getWork/:id", (req, res, next) => {
    
    var sql = "select subject.name as matiereName,WorkSession.name,WorkSession.id,WorkSession.openingDate,WorkSession.timeSpend,WorkSession.subject_id from WorkSession ,"+
    "subject where WorkSession.subject_id == subject.id and WorkSession.id == "+req.params.id+" "

    db.all(sql, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});
app.get("/api/getWork/semaine/:id", (req, res, next) => {
    
    var sql = "select subject.name as matiereName,WorkSession.name,WorkSession.semaine,WorkSession.id,WorkSession.openingDate,WorkSession.timeSpend,WorkSession.subject_id from WorkSession ,"+
    "subject where WorkSession.subject_id == subject.id and WorkSession.semaine == "+req.params.id+" "

    db.all(sql, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});
app.post("/api/createSubject/", (req, res, next) => {
    var errors=[]
    // if (!req.body.password){
    //     errors.push("No password specified");
    // }
    // if (!req.body.email){
    //     errors.push("No email specified");
    // }
    // if (errors.length){
    //     res.status(400).json({"error":errors.join(",")});
    //     return;
    // }
    var data = {
        name: req.body.name,
    }
    var sql ='INSERT INTO subject (name) VALUES (?)'
    var params =[data.name]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

app.post("/api/createSession/", (req, res, next) => {
    var errors=[]
    // if (!req.body.password){
    //     errors.push("No password specified");
    // }
    // if (!req.body.email){
    //     errors.push("No email specified");
    // }
    // if (errors.length){
    //     res.status(400).json({"error":errors.join(",")});
    //     return;
    // }
    var data = {
        name: req.body.name,
        openingDate: req.body.openingDate,
        timeSpend : req.body.timeSpend,
        semaine:req.body.semaine,
        subject_id:req.body.subject_id 
    }
    var sql ='INSERT INTO worksession (name, openingDate,semaine, timeSpend,subject_id) VALUES (?,?,?,?,?)'
    var params =[data.name, data.openingDate,data.semaine, data.timeSpend,data.subject_id]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})


app.delete("/api/deleteSession/:id", (req, res, next) => {
    db.run(
        'DELETE FROM WorkSession WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", rows: this.changes})
    });
})

app.delete("/api/deleteSubject/:id", (req, res, next) => {
    db.run(
        'DELETE FROM subject WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", rows: this.changes})
    })
})

app.patch("/api/updateSession/:id", (req, res, next) => {
    var data = {
        timeSpend: req.body.timeSpend,
        name: req.body.name,
        semaine:req.body.semaine,
        subject_id: req.body.subject_id,
    }

    db.run(
        `UPDATE worksession set 
           timeSpend = ?,
           name = ?,
           semaine = ?,
           subject_id = ?
           WHERE id = ?`,
        [data.timeSpend,data.name,data.semaine,data.subject_id, req.params.id],
        
        function (err, result) {           
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})
app.patch("/api/updateSessionTime/:id", (req, res, next) => {
    var data = {
        timeSpend: req.body.timeSpend,
    }

    db.run(
        `UPDATE worksession set 
           timeSpend = ?
            WHERE id = ?`,
        [data.timeSpend,req.params.id],
        
        function (err, result) {           
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})
// Root path
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});
