CREATE TABLE IF NOT EXISTS Subject(
	id INTEGER,
	name VARCHAR2(100), --nom de la matière
	CONSTRAINT PK_Subject PRIMARY KEY (id)
	);
	
CREATE TABLE IF NOT EXISTS WorkSession(
	id INTEGER,
	name VARCHAR2(100), --nom du TP
	openingDate DATETIME, --date de création de la session
	timeSpend TIME, --temps de travail total
	semaine INTEGER,
	subject_id INTEGER,
	
	CONSTRAINT PK_WorkSession PRIMARY KEY (id),
	CONSTRAINT FK_WorkSession_Subject FOREIGN KEY (subject_id) REFERENCES Subject (id)
	);