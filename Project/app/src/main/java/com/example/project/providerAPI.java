package com.example.project;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.future.ResponseFuture;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class providerAPI {
    int port = 8000 ;
    String adr = "89.90.179.90";
    String url = "http://localhost:"+port+"";
    Context c ;

    providerAPI(Context c){
    this.c = c;
    }

    public ResponseFuture<JsonObject> getSession(){
        return Ion.with(c).load("http://"+adr+":"+port+"/api/getAllSession").asJsonObject();
    }

    public ResponseFuture<JsonObject> getSubject(){
        return Ion.with(c).load("http://"+adr+":"+port+"/api/getAllSubject").asJsonObject();
    }
    public ResponseFuture<JsonObject> getWork(){
        return Ion.with(c).load("http://"+adr+":"+port+"/api/getWork").asJsonObject();
    }
    public ResponseFuture<JsonObject> getWorkSemaine(int week){
        return Ion.with(c).load("http://"+adr+":"+port+"/api/getWork/semaine/"+week+"").asJsonObject();
    }
    public ResponseFuture<JsonObject> getWork(int id){
        return Ion.with(c).load("http://"+adr+":"+port+"/api/getWork/"+id).asJsonObject();
    }
    public void createSubject(String name){
        JsonObject json = new JsonObject();
        json.addProperty("name", name );
        Ion.with(c)
                .load("http://"+adr+":"+port+"/api/createSubject")
                .setJsonObjectBody(json)
                .asJsonObject();
    }
    public void createSession(String name,long openingDate,long timeSpend,int semaine,int subject_id){
        JsonObject json = new JsonObject();
        json.addProperty("name", name );
        json.addProperty("openingDate", openingDate );
        json.addProperty("timeSpend", timeSpend );
        json.addProperty("semaine", semaine );
        json.addProperty("subject_id", subject_id );
        Ion.with(c)
                .load("http://"+adr+":"+port+"/api/createSession")
                .setJsonObjectBody(json)
                .asJsonObject();
    }
    public void deleteSubject(int id){
        Ion.with(c).load("DELETE","http://"+adr+":"+port+"/api/deleteSubject/"+id+"").asJsonObject();
    }
    public void deleteSession(int id){
        Ion.with(c).load("DELETE","http://"+adr+":"+port+"/api/deleteSession/"+id+"").asJsonObject();
    }
    public void updateSession(String nameTP,String time,int subject_id,int id){
        JsonObject json = new JsonObject();
        json.addProperty("name", nameTP);
        json.addProperty("timeSpend", time);
        json.addProperty("subject_id", subject_id );
        Ion.with(c)
                .load("PATCH","http://"+adr+":"+port+"/api/updateSession/"+id+"")
                .setJsonObjectBody(json)
                .asJsonObject();
    }
    public void updateSessionTime(long time,int id){
        JsonObject json = new JsonObject();
        json.addProperty("timeSpend", time);
        Ion.with(c)
                .load("PATCH","http://"+adr+":"+port+"/api/updateSessionTime/"+id+"")
                .setJsonObjectBody(json)
                .asJsonObject();
    }

}
