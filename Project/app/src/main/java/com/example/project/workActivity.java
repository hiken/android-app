package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class workActivity extends AppCompatActivity {
    Button bSuivie,bValider;
    EditText dateJ,matiere,tp;
    TimePicker Tp;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    String date = sdf.format(new Date());
    providerAPI pApi ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_work);


        bSuivie = findViewById(R.id.button5);
        bValider = findViewById(R.id.valider);
        matiere = findViewById(R.id.matiere);
        tp = findViewById(R.id.TP);
        dateJ = findViewById(R.id.date);
        dateJ.setText(date);
        //Tp = findViewById(R.id.datePicker1);
        //Tp.setIs24HourView(true);
        pApi = new providerAPI(getApplication());
    }


    public void onClick(View v ){
        if (v.getId()==bSuivie.getId()){
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            finish();
            startActivity(i);
        }
        if (v.getId()==bValider.getId()){
            pApi.getSubject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e,JsonObject result) {
                    try{
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(sdf.parse(String.valueOf(dateJ.getText())));
                        int week = cal.get(Calendar.WEEK_OF_YEAR);
                        Log.d("date", "nb semaine = " +week);

                        String subject = result.get("data").toString();
                        JSONArray jsonArraySubject = new JSONArray(subject);
                        boolean trouve;
                        for (int i=0;i<jsonArraySubject.length();i++){
                            JSONObject jsonElementSubject = jsonArraySubject.getJSONObject(i); // get les elements du jsonArray
                            trouve = matiere.getText().toString().toLowerCase().equals(jsonElementSubject.get("name").toString().toLowerCase());
                            // Log.d("debug","boolean " + trouve +"FIRST LOOP GET SESSION"+ jsonElementSubject.get("name").toString() +
                            //         " FIRST LOOP compteur: " +i + " list des sujets : " + jsonArraySubject.length());
                            if (trouve){
                                // cas true => le sujet a été crée dans la db donc pas besoin de le créer
                                // il faut uniquement créer la session 
                                pApi.createSession(tp.getText().toString(),new Date().getTime(),0,week,(Integer)jsonElementSubject.get("id"));
                                break;
                            }
                            else if ( i  == (jsonArraySubject.length() -1) ){
                                // cas false si le sujet n'est pas crée alors on le crée et on crée la session
                                // Log.d("debug","OBJECT here " + matiere.getText().toString() + " " + tp.getText().toString());
                                pApi.createSubject(matiere.getText().toString());
                                pApi.createSession(tp.getText().toString(),new Date().getTime(),0,week,jsonArraySubject.length() +1);
                            }
                         }
                        Intent i = new Intent(getApplicationContext(),MainActivity.class);
                        finish();
                        startActivity(i);
                    }catch (Exception err){
                        Log.e("debug",""+err);
                    }

                }
            }) ;

        }
    }
}
