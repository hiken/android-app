package com.example.project;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Chronometer;

public class monMinuteur extends CountDownTimer {

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    long TpsI;//nombre de milliseconde au départ
    long TpsR;//nombre de milliseconde restantes
    Chronometer refChrono;
    public monMinuteur(long millisInFuture, long countDownInterval, Chronometer ref) {
        super(millisInFuture, countDownInterval);
        this.TpsI = millisInFuture;
        this.refChrono =  ref;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        this.setTpsR(millisUntilFinished);

        this.refChrono.setText(formatTime(this.TpsR/1000));
        Log.d("debug", String.valueOf(this.TpsR/1000));
    }
    public String formatTime(long millTotal){
        int sec = (int) (millTotal%60);
        int minutes = (int) ((millTotal%3600)/60);
        int hours = (int) (millTotal/3600);
        String h;
        String m;
        String s;
        if(hours<10){
            h = "0"+hours;
        }else{
            h = String.valueOf(hours);
        }
        if(minutes<10){
            m = "0"+minutes;
        }else{
            m = String.valueOf(minutes);
        };
        if(sec<10){
            s = "0"+sec;
        }else{
            s = String.valueOf(sec);
        }


        if (hours<1){

            return m + " : " + s;
        }

        return h +" : "+m + " : " + s;
    }

    @Override
    public void onFinish() {
        Log.d("debug", String.valueOf(this.TpsI/1000));
    }

    public long getTpsR(){
        return this.TpsR;
    }

    public long getTpsI(){
        return this.TpsI;
    }

    public void setTpsR(long time){
        this.TpsR = time;
    }
}
