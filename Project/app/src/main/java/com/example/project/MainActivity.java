package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.parser.JSONObjectParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button bCreateSession,bCreate ;
    TableLayout tl ;
    LinearLayout ll ;
    providerAPI pApi ;
    Spinner spinner ;
    int week = -1;
    String myPrefsName= "mesPreferences";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bCreateSession = findViewById(R.id.button6);
        tl = findViewById(R.id.mainTL);
        spinner = findViewById(R.id.spinner);
        ll = findViewById(R.id.mainLayout);
        initSpinner();
        semaineActive();
        pApi = new providerAPI(getApplication());
    }
    public void initSpinner(){
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        for(int i=1;i<55;i++){
            spinnerAdapter.add("S"+i);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                SharedPreferences.Editor editor = getSharedPreferences(myPrefsName, MODE_PRIVATE).edit();

                int count = tl.getChildCount();
                for (int i = 1; i < count; i++) {
                    View child = tl.getChildAt(i);
                    if (child instanceof TableRow) ((TableRow) child).removeAllViews();
                }
                int selectedItem = parent.getSelectedItemPosition();
                week = selectedItem +1 ;
                editor.putInt("week", week);
                editor.apply();
                Log.d("debug","go "+selectedItem);
                ll.setVisibility(View.GONE);
                ll.setVisibility(View.VISIBLE);
                initTable();

            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
        spinnerAdapter.notifyDataSetChanged();
    }
    public void initTable(){
        pApi.getSubject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e,JsonObject result) {
                try{
                    String joke = result.get("data").toString();
                    JSONArray jsonArrayData = new JSONArray(joke);
                    addContentFromAPI(jsonArrayData);
                }catch (Exception err){
                    Log.e("debug",""+err);
                }

            }
        }) ;
    }
    public void semaineActive(){
        Calendar calender = Calendar.getInstance();
        SharedPreferences prefs = getSharedPreferences(myPrefsName, MODE_PRIVATE);
        week= prefs.getInt("week", calender.get(Calendar.WEEK_OF_YEAR)-1); //0 is the default value.
        spinner.setSelection(week-1);
    }
    private void addContentFromAPI(JSONArray jsonArrayData){
        final JSONArray jsonArray = jsonArrayData ; // liste
        Log.d("debug"," in addContent "+ week);
        pApi.getWorkSemaine(week).setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                try{

                    String data = result.get("data").toString();
                    JSONArray jsonArrayData = new JSONArray(data);
                    for (int i = 0 ; i<= jsonArrayData.length() ; i++){
                        JSONObject listWork = jsonArrayData.getJSONObject(i); // get les elements du jsonArray => list des travaux
                        for (int j=0 ; j<jsonArray.length();j++){
                            JSONObject jsonlistSubject = jsonArray.getJSONObject(j); // get les elements du jsonArray => list subject
                            if (jsonlistSubject.get("id").equals(listWork.get("subject_id"))) {
                                createRow(jsonlistSubject.get("name").toString(),
                                        listWork.get("name").toString(),
                                        HH_MM_SS(((int)listWork.get("timeSpend"))),
                                        (int)listWork.get("id"));

                            }
                        }
                    }

                }catch (Exception err){
                    Log.e("debug",""+err);
                }
            }

        });

    }
    public void onClick(View v ){
        if (v.getId()==bCreateSession.getId()){
            Intent i = new Intent(getApplicationContext(),workActivity.class);
            finish();
            startActivity(i);
        }
    }
    public void createRow(String name, String nameTP, String date, final int id){
        final int ind = id ;
        //Log.d("debug","id : before click" + ind);
        TableRow tb = new TableRow(this);
        TextView tvMatiere,tvTemps,tvTP,tvStatus  ;
        Button bAction = new Button(this);
        tvMatiere  = new TextView(this);
        tvTP = new TextView(this);
        tvTemps = new TextView(this);
        tvStatus = new TextView(this);
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 1f);

        tvMatiere.setText(name);
        tvTP.setText(nameTP);
        tvTemps.setText(date);
        tvStatus.setText("En cours");
        bAction.setText("Supprimer");
        tvMatiere.setLayoutParams(params);
        tvTP.setLayoutParams(params);
        tvTemps.setLayoutParams(params);
        tvStatus.setLayoutParams(params);
        bAction.setLayoutParams(params);

        tvMatiere.setGravity(Gravity.CENTER);
        tvTP.setGravity(Gravity.CENTER);
        tvTemps.setGravity(Gravity.CENTER);
        tvStatus.setGravity(Gravity.CENTER);
        bAction.setGravity(Gravity.CENTER);

        tvMatiere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToSession(id);
            }
        });
        tvTemps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToSession(id);
            }
        });
        tvTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToSession(id);
            }
        });
        tvStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToSession(id);
            }
        });
        bAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pApi.deleteSession(ind);
                finish();
                // pour refresh sans l'animation lente d'android studio
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        tb.addView(tvMatiere);
        tb.addView(tvTP);
        tb.addView(tvTemps);
        tb.addView(tvStatus);
        tb.addView(bAction);
        tl.addView(tb);

    }

    public void navigateToSession(int id){
        Intent i = new Intent(getApplicationContext(),SessionActivity.class);
        //Log.d("debug","id:"+ id);
        i.putExtra("id",id);
        finish();
        startActivity(i);
    }
    public static String HH_MM_SS(int timeSpend){
        long h = timeSpend/3600;
        long m = (timeSpend%3600)/60;
        long s = (timeSpend%60);
        String HH_MM_SS="";
        if(h<10){HH_MM_SS+="0"+h+":";}else{HH_MM_SS+=h+":";}
        if(m<10){HH_MM_SS+="0"+m+":";}else{HH_MM_SS+=m+":";}
        if(s<10){HH_MM_SS+="0"+s;}else{HH_MM_SS+=s;}
        return HH_MM_SS;
    }
}
