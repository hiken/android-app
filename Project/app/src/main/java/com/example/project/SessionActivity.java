 package com.example.project;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Calendar;

public class SessionActivity extends AppCompatActivity {
    Button bCreer,bSuivie,start,reset;
    Chronometer chronometer ;
    monMinuteur m;
    TimePicker tp ;
    TableLayout tl ;
    providerAPI pApi ;
    Integer id = null;
    boolean isStarted = false;
    JSONObject jsonObjectWork = null;// id pour pApi
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        pApi = new providerAPI(getApplication());
        bSuivie = findViewById(R.id.button5);
        bCreer = findViewById(R.id.button6);

        start = findViewById(R.id.start); // button pour chrono
        reset = findViewById(R.id.reset);
        tl = findViewById(R.id.tl);


        chronometer = findViewById(R.id.chrono);
        tp = findViewById(R.id.datePicker1);
        tp.setIs24HourView(true);
        id = getIntent().getExtras().getInt("id");

        pApi.getWork(id).setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e,JsonObject result) {
                try{
     //               Log.d("debug","fdp");
                    String joke = result.get("data").toString();
                    JSONArray ja = new JSONArray(joke);
      //              Log.d("debug","ja"+ ja.toString());
                    JSONObject jsonListWorkElement = ja.getJSONObject(0); // get les elements du jsonArray
                    jsonObjectWork = jsonListWorkElement ;
                    createRow(jsonListWorkElement.getString("matiereName"),jsonListWorkElement.getString("name"),jsonListWorkElement.getString("timeSpend"));
                }catch (Exception err){
                    Log.e("debug",""+err);
                }

            }
        }) ;

    }



    public void onClick (View v ){
        if (v.getId()==bSuivie.getId()){
            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            finish();
            startActivity(i);
        }

        if (v.getId()==bCreer.getId()){
            Intent i = new Intent(getApplicationContext(),workActivity.class);
            finish();
            startActivity(i);
        }
    }
    public void createRow(String name, String nameTP, String date){
        TableRow tb = new TableRow(this);
        final EditText tvMatiere,tvTemps,tvTP  ;
        TextView tvStatus;
        Button bAction = new Button(this);
        tvMatiere  = new EditText(this);
        tvTP = new EditText(this);
        tvTemps = new EditText(this);
        tvStatus = new TextView(this);
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 1f);

        tvMatiere.setText(name);
        tvTP.setText(nameTP);
        tvTemps.setText(date);
        tvStatus.setText("En cours");
        bAction.setText("Modifier");
        tvMatiere.setLayoutParams(params);
        tvTP.setLayoutParams(params);
        tvTemps.setLayoutParams(params);
        tvStatus.setLayoutParams(params);
        bAction.setLayoutParams(params);

        tvMatiere.setGravity(Gravity.CENTER);
        tvTP.setGravity(Gravity.CENTER);
        tvTemps.setGravity(Gravity.CENTER);
        tvStatus.setGravity(Gravity.CENTER);
        bAction.setGravity(Gravity.CENTER);

        tvTemps.setInputType(InputType.TYPE_CLASS_NUMBER);
        bAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // providerApi
                // modifier case
                /*
                 * if user input doesn't exist in db = > create
                 * donc creer un nouveau sujet et changer le subject id du work
                 * then call modif
                 * ou si sujet deja existant => juste call modif
                 * */
                pApi.getSubject().setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e,JsonObject result) {
                        try{
                            int index = -1 ;
                            String dataSubject = result.get("data").toString();
                            JSONArray ja = new JSONArray(dataSubject);
                            Log.d("debug","ja Array" + ja.length() + " tabs "+ ja.toString());
                            index = findSubject(ja,tvMatiere.getText().toString());
                            Log.d("debug","index :" +index + " " + jsonObjectWork.toString());
                            if (index == -1){
                                pApi.createSubject(tvMatiere.getText().toString());
                                pApi.updateSession(tvTP.getText().toString(),tvTemps.getText().toString(),(Integer) ja.length() +1,id);

                            }else{

                                pApi.updateSession(tvTP.getText().toString(),tvTemps.getText().toString(),index,id);

                            }
                            createAlert();
                        }catch (Exception err){
                            Log.e("debug",""+err);
                        }

                    }
                }) ;


            }
        });

        tb.addView(tvMatiere);
        tb.addView(tvTP);
        tb.addView(tvTemps);
        tb.addView(tvStatus);
        tb.addView(bAction);
        tl.addView(tb);

    }
    private int findSubject(JSONArray jsonArray,String name ){
        try{

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonListWorkElement = jsonArray.getJSONObject(i); // get les elements du jsonArray
                Log.d("debug","NAME : "+jsonListWorkElement.get("name"));
                if(jsonListWorkElement.get("name").toString().equals(name)){
                    Log.d("debug","same name " +jsonListWorkElement.get("name").toString() + " " + name);

                    return (int) jsonListWorkElement.get("id");
                }else{
                    Log.d("debug","not same name "+jsonListWorkElement.get("name").toString()+ " " + name );

                }
            }
        }catch (Exception err){
            Log.e("debug",""+err);
        }
        return -1;

    }
    public void createAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Modification");
        builder.setMessage("La modification de votre session de travail a ete effectue");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
    @SuppressLint("WrongConstant")
    public void onClickStart(View v){
        try{
            isStarted = !isStarted;
            if (isStarted){
                reset.setVisibility(View.GONE); // hidden sans qu'il influe sur les composants graphiques (gone)0
                tp.setVisibility(View.GONE);
                chronometer.setVisibility(View.VISIBLE);
                m=new monMinuteur((tp.getCurrentHour()*3600 + tp.getCurrentMinute()*60)*1000, 1000, chronometer );
                m.start();
                start.setText("stop");

            }else{
                pApi.updateSessionTime((int)jsonObjectWork.get("timeSpend")+(m.getTpsI() - m.getTpsR())/1000,id);
                Log.d("temps_travail",String.valueOf((m.getTpsI() - m.getTpsR())/1000));

                m.cancel();
                start.setText("start");
                Log.d("debug",""+reset.getVisibility());
                reset.setVisibility(View.VISIBLE); // visible
                tp.setVisibility(View.VISIBLE);
                chronometer.setVisibility(View.GONE);

            }
        }catch (Exception err){
            Log.e("debug",""+err);
        }

    }

    public void onClickDate(View v){


    }
}
